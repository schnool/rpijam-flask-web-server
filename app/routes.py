"""
Define routes functions
"""
from flask import render_template, request
from app import app
#import RPi.GPIO as GPIO
import time

# Define the gpio mode
# GPIO.setmode(GPIO.BCM)

# Define pins
red = 21
green = 20
blue = 22

# Define pins setup
GPIO.setup(red, GPIO.OUT)
GPIO.setup(green, GPIO.OUT)
GPIO.setup(blue, GPIO.OUT)

Freq = 100

# defining the pins that are going to be used with PWM
RED = GPIO.PWM(red, Freq)
GREEN = GPIO.PWM(green, Freq)
BLUE = GPIO.PWM(blue, Freq)

RED.start(100)
GREEN.start(1)
BLUE.start(1)


@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/led', methods=['GET', 'POST'])
def led():
    # Leds default value
    red_value = 50
    green_value = 50
    blue_value = 50
    if request.method == 'POST':
        red_value = request.form['redLed']
        green_value = request.form['greenLed']
        blue_value = request.form['blueLed']
        RED.ChangeDutyCycle(red_value)
        GREEN.ChangeDutyCycle(green_value)
        BLUE.ChangeDutyCycle(blue_value)
    return render_template('led.html',
                           red=red_value,
                           green=green_value,
                           blue=blue_value)


@app.route('/servo')
def servo():
    return render_template('servo.html')


@app.route('/word')
def word():
    return render_template('word.html')
